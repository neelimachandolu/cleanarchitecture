﻿using CleanArchitecture.Core.Entities;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace CleanArchitecture.Infrastructure.TeamCityAPI
{
    public class TeamCityAPIClient
    {
        FlurlClient Client = new FlurlClient();
        public async Task<Builds> GetBuildInformation()
        {
            // string path = "http://72.52.217.118:88/app/rest/ui/builds?locator=item%3A(buildType%3ANchandoluTestproject_Build%2Cbranch%3A(default%3Aany)%2Ccount%3A15)&fields=nextHref,count,build(id,number,branchName,defaultBranch,startDate,finishDate,statusText,status,user(id,name,username))";
            string path = "app/rest/ui/builds";
            //string path = "app/rest/ui/builds?locator=item%3A(buildType%3ANchandoluTestproject_Build%2Cbranch%3A(default%3Aany)%2Ccount%3A15)&fields=nextHref,count,build(id,number,branchName,defaultBranch,startDate,finishDate,statusText,status,user(id,name,username))";
            //var query = new
            //{
            //    locator = Url.Encode("item:(buildType:NchandoluTestproject_Build,branch:(default:any),count:15))"),
            //    fields= "nextHref,count,build(id,number,branchName,defaultBranch,startDate,finishDate,statusText,status,user(id,name,username))"
            //};

           return await "http://72.52.217.118:88/"
                .AppendPathSegment(path,false)
                .SetQueryParam("locator", "item%3A(buildType%3ANchandoluTestproject_Build%2Cbranch%3A(default%3Aany)%2Ccount%3A15)",true)
                .SetQueryParam("fields", "nextHref,count,build(id,number,branchName,defaultBranch,startDate,finishDate,statusText,status,user(id,name,username))",true)
                //.SetQueryParams(query)
                //.SetQueryParam("locator", "item%3A(buildType%3ANchandoluTestproject_Build%2Cbranch%3A(default%3Aany)%2Ccount%3A15))")
                //.SetQueryParam("fields", "nextHref,count,build(id,number,branchName,defaultBranch,startDate,finishDate,statusText,status,user(id,name,username))")
                .WithHeader("Accept", "application/json")               
                .WithBasicAuth("Neelima", "Password12345")
                .GetJsonAsync<Builds>();
            
        }
    }
}
