﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.TeamCityAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace CleanArchitecture.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public BuildInfo[] Builds { get; set; }
        public void OnGet()
        {
            var client = new TeamCityAPIClient();
            //Build = client.GetBuildInformation().Result?.Build.OrderByDescending(o => o.Number).FirstOrDefault();
            Builds = client.GetBuildInformation().Result?.Build.OrderByDescending(o => o.Number).ToArray();
        }
    }
}
