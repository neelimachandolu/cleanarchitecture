﻿using CleanArchitecture.Core.Entities;
using System.Collections.Generic;

namespace CleanArchitecture.Core.Interfaces
{
    public interface IStudyRepository
    {
        IList<Study> GetStudies();
    }
}