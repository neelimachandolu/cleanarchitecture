﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CleanArchitecture.Core.Entities
{
	public class BuildInfo
    {
		public string StatusText { get; set; }
		public string StartDate { get; set; }
		public string FinishDate { get; set; }
		public int Id { get; set; }
		public int Number { get; set; }
		public string Status { get; set; }
	}

	public class Builds
	{
		public int Count { get; set; }

		public BuildInfo[] Build { get; set; }
	}
}
