﻿using System;

namespace CleanArchitecture.Core.Entities
{
    public class Study
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MinParticipants { get; set; }
    }
}